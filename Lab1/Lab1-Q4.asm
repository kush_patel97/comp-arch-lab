.data
str: .asciiz "Enter your name: "
greet_1: .asciiz "Hello "
greet_2: .asciiz "please input an integer and press Enter\n"
goodbye: .asciiz "Goodbye "
ty: .asciiz  "Thank you for your input"
even_val: .asciiz "The integer is even\n"
odd_val: .asciiz "The integer is odd\n"
name: .space 20
int_input: .space 9999

.text
	#.globl main

main:
li $v0, 4  #address where to store string input
la $a0, str #the max length
syscall
#read int
li $v0, 8
la $a0, name
li $a1, 20
syscall
#print greet 1
li $v0, 4
la $a0, greet_1
syscall
# print name
li $v0, 4
la $a0, name
syscall
#print greet 2
li $v0, 4
la $a0, greet_2
syscall
# read int
li $v0, 5
syscall
# set int == to temp var
move $t0, $v0
#find remainer
rem $t1, $t0, 2
beq $t1, $zero, if

li $v0, 4
la $a0, odd_val
syscall
b closing_statement

if:
li $v0, 4
la $a0, even_val
syscall

closing_statement:
li $v0, 4
la $a0, goodbye
syscall		

li $v0, 4
la $a0, name
syscall		

li $v0, 4
la $a0, ty
syscall		

li $v0, 10
syscall


.data

name: .space 25
prompt: .asciiz "\nPlease enter your name: "
hello: .asciiz "\nHello "
input: .asciiz "Please input an integer and press Enter "
goodbye: .asciiz "\nGoodbye "
thanku: .asciiz "Thank you for your input"
oddmsg: .asciiz "The integer is odd \n"
evenmsg: .asciiz "The integer is even\n"
.text
main:

#prompt for name and get string input
li $v0, 4
la $a0, prompt
syscall
li $v0, 8
la $a0, name #address where to stroe string input
li $a1, 25 #the max length
syscall

#greet the user and prompt for int input
li $v0, 4
la $a0, hello
syscall

#display name
li $v0, 4
la $a0, name
syscall

li $v0, 4
la $a0, input
syscall

#get int input and save to $t0
li $v0, 5
syscall
move $t0, $v0

#find out if the number is odd or even. div by 2 and if remainder is 0, its even
li $t1, 2 #load the number 2 into $t1
div $t0, $t1 #quotient is in low and remainder in hi
mfhi $t1 #get the remainder into t1
beqz $t1, display_even

#display odd msg
li $v0, 4
la $a0, oddmsg
syscall
b finish
display_even:

#display even msg
li $v0, 4
la $a0, evenmsg
syscall
finish:

#say good bye
li $v0, 4
la $a0, goodbye
syscall
li $v0, 4
la $a0, name
syscall

li $v0, 4
la $a0, thanku
syscall

#exit
li $v0, 10
syscall
.data 
num1: .asciiz "Enter the first number: \n"
num2: .asciiz "Enter the second number: \n"

.text
main:
li $v0, 4			# Prints out num1 string using syscall
la $a0, num1		
syscall				

li $v0, 5			# Reads in an input
syscall				
move $t1, $v0		# Moves the input to a temp register

li $v0, 4			# Prints out num2 string using syscall
la $a0, num2
syscall

li $v0, 5			# Reads in an input
syscall
move $t2, $v0		# moves this input to another, different temp register


blt $t1, $t2, if	# if t1 is less than t2 branch to if

li $v0, 1			# prints out the value of the lower string
move $a0, $t2
syscall
b exit

if:					# Prints out the value of the lower string
li $v0, 1
move $a0, $t1
syscall

exit:				# exits the program
li $v0, 10
syscall

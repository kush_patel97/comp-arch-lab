.data
greet_2: .asciiz "Please input an integer and press Enter\n"
even_val: .asciiz "The integer is even\n"
odd_val: .asciiz "The integer is odd\n"
int_input: .space 9999

.text
	#.globl main

main:
#print greet 2
li $v0, 4
la $a0, greet_2
syscall
# read int
li $v0, 5
syscall
# set int == to temp var
move $t0, $v0

rem $t1, $t0, 2			#find remainer
beq $t1, $zero, if		#if t1 is less than 0 branch to if

li $v0, 4				# Prints out odd statement if branch doesn't occur
la $a0, odd_val
syscall
b closing_statement

if:						# Prints out even statement
li $v0, 4
la $a0, even_val
syscall

closing_statement:
li $v0, 10
syscall


.data
str: .asciiz "Enter a number for fibonacchi: \n"
.text
main:
li $v0, 4
la $a0, str
syscall

li $v0, 5
syscall
move $t0, $v0		# get input and store in $t0
				#Initialize counter to 0		
beqz $t0, zero		#if input == zero go to zero
beq $t0, 1, one		#if input == 1 go to one

li $t5, 1
# t1 = fib
#t2 = num1
#t3 = num2
li $t2, 0
li $t3, 1

loop:
beq $t5, $v0, endloop 	# Break loop when counter == input
add $t1, $t2,$t3		# Fib = num1 + num2
add $t2, $t3, 0			# num1 = num2
add $t3, $t1, 0			# num2 = fib
add $t5, $t5, 1			# increment counter
j loop					# go to the loop again

zero:					# Print out zero
li $v0, 1				 
move $a0, $t1			
syscall					
b endloop				 

one:					# Print out 1
li $v0, 1
move $a0, $t1
syscall
li $v0, 10
syscall
	
endloop:			# Print out values and end the program
li $v0, 1
move $a0, $t1
syscall
li $v0, 10
syscall



.data
str: .asciiz "Enter a number from 1000 to 9999: \n"
.text
main:
li $v0, 4
la $a0, str
syscall

li $v0, 5		#read in an integer	
syscall			
move $t0, $v0 	# t0 = num

li $t1, 0		# t1 = sum = 0
add $t2, $zero, 10

blt $t0, 1000, err
bgt $t0, 9999, err

loop:
ble $t0, $zero, end		# If num < 0, break loop
div $t0, $t2			# Divide num by 10
mflo $t3				# Put the quotient in t3 
mfhi $t4				# Put the remainder in t4
add $t1, $t1, $t4 			# Set sum = sum + remainder($t4)
move $t0, $t3				# Set num = num/10
j loop

err:				# If the number is less than 1000 or greater than 9999 then ask the user to input new values
li $v0, 4
la $a0, str
syscall
li $v0, 5
syscall	
move $t0, $v0
b loop

end:
move $a0, $t1
li $v0, 1
syscall
li $v0, 10
syscall



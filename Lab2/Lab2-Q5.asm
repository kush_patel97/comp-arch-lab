.data
str: .asciiz "Enter a number: \n"
.text

main:
li $v0, 4
la $a0, str
syscall

li $v0, 5					#read in an integer	
syscall			
move $t0, $v0 				# t0 = num

li $t1, 0					# t1 = sum = 0
li $t2, 10					# t2 = 10


loop:
	ble $t0, $zero, check	# If num < 0, break loop
	div $t0, $t2			# Divide num by 10
	mflo $t3				# Put the quotient in t3 
	mfhi $t4				# Put the remainder in t4
	add $t1, $t1, $t4 			# Set sum = sum + remainder($t4)
	move $t0, $t3				# Set num = num/10
	j loop

check:
	move $t0, $t1		# Move sum to t0 = num
	sub $t1, $t1, $t1 
	blt $t0, 10, end		# if num is less than 10 end the loop
	bgt $t0, 10, loop		# if num is greater than 10 go back to the loop

end:
	move $a0, $t0
	li $v0, 1
	syscall
	li $v0, 10
	syscall



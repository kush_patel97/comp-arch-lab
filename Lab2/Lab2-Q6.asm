.data

	str: 			.asciiz "Enter the the students recent attendence: \n"
	true: 			.asciiz "True"
	false: 			.asciiz "False"

	attendence: 	.space 25
.text

main:	
	li $v0, 4				
	la $a0, str				
	syscall					

	li $v0, 8				#Reads in a string input and creates a space for a maximum of 25 characters
	la $a0, attendence		
	li $a1, 25				
	syscall					
	move $t3, $zero			#moves value of 0 into register $t3 to initialize
	move $t2, $zero			#moves value of 0 into register $t2 to initialize
	move $t1, $zero			#moves value of 0 into register $t1 to initialize

loop:
	lb $t0, ($a0)			#loads bit from $a0 to $t0
	add $a0, $a0, 1			#increments a0 by 1 each time this loop runs
	beq $t0, 'A', plust1	#if t0 == A branch to plust1
	beq $t0, 'L', plust2	#if t0 == L branch to plust2
	beqz $t0, res			#branches to res if $t0 is equal to 0
	b loop					#branch back to loop

plust1:
	add $t1, $t1, 1 		# increment by 1
	b loop					# branch to loop

plust2:
	add $t2, $t2, 1			#increment by 1
	b loop					#branch to the loop

res:
	bgt $t2, 2, iffalse		# if $t2 is greater than 2 branch to iffalse
	bgt $t1, 1, iffalse		# if $t1 is greater than 1 branch to iffalse

	li $v0, 4				#Print out the true statement
	la $a0, true			
	syscall					
	b exit

iffalse:
	li $v0, 4				#Print out False statement
	la $a0, false			
	syscall					

exit:
	li $v0, 10				



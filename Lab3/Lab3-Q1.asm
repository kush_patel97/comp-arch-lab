.data 
A: .asciiz "Enter the first number: \n"
B: .asciiz "Enter the second number: \n"
C: .asciiz "Enter the third number: \n"

.text
main:
li $v0, 4
la $a0, A
syscall
		
li $v0, 5	
syscall	
move $t0, $v0		# Moves A into v0 

li $v0, 4
la $a0, B
syscall
		
li $v0, 5	
syscall	
move $t1, $v0		# Moves B into v0

li $v0, 4
la $a0, C
syscall
		
li $v0, 5	
syscall	
move $t2, $v0		# Moves C into v0

#-------------------------------------------------
# F=((NOT A) OR C)' AND (A AND B)'
# t0 = A
# t1 = B
# t2 = C
# t4 = F

not $t5, $t0		# t5 = Not A
or $t5, $t5, $t2	# t5 = ((NOT A) OR C)
not $t5, $t5 		# t5 = ((NOT A) OR C)'

and $t6, $t0, $t1	# t6 = A AND B
not $t6, $t6		# t6 = (A AND B)'
and $t4, $t5, $t6	# F=((NOT A) OR C)' AND (A AND B)'

li $v0, 1			# Prints answer F
move $a0, $t4
syscall

li $v0, 10
syscall
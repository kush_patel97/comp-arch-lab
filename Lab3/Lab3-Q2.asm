.data 
num1: .asciiz "Enter the first number: \n"
num2: .asciiz "Enter the second number: \n"
errmsg: .asciiz "input range error, Try again \n"
limitmsg: .asciiz "\nerror limit exceeded\n"
result: .asciiz "Result: "

.text
main:
li $v0, 4		# Prints num1 message
la $a0, num1
syscall
		
li $v0, 5		# Reads in input and moves it to register t0
syscall	
move $t0, $v0

blt $t0, 1, err 		# If the number is less than 1 or greater than 1000 then ask the user to input new values
bgt $t0, 1000, err

li $v0, 4		# Prints num2 message
la $a0, num2
syscall
		
li $v0, 5		# Reads in input and moves it to register t1
syscall	
move $t1, $v0

blt $t1, 1, err 	#If the number is less than 1 or greater than 1000 then ask the user to input new values
bgt $t1, 1000, err
#li $t2, 0


# t0 = multiplier
# t1 = multiplicand
# t2 = product

li $t2, 0
loop:						# This loop method checks it the least significant bit of the multiplier is 0 or 1
	beqz $t1, end			
	andi $s0, $t0, 1		# Stores the least significant bit of the multiplier in register s0
	beq $s0, 1, addshifts	# If 1, branch to addShifts
	beq $s0, 0, justshift	# If 0, branch to justshift
	j loop

addshifts:					
	add $t2, $t2, $t1		# adds the multiplicand(t1) to the product(t2)
	sllv $t1, $t1, 1		# shifts multiplicand(t1) left 1 bit
	srlv $t0, $t0, 1		# shifts multiplier(t0) right 1 bit
	j loop					# Jumps back to loop to continue with program

justshift:					# Just shifts both bits without any arithmetic operations
	sllv $t1, $t1, 1		# shifts multiplicand(t1) to the left 1 bit
	srlv $t0, $t0, 1		# shifts multiplier(t0) to the right 1 bit
	j loop					# Jumps back to loop to continue with program


err:
	beq $t3, 2, limit		# If the user has inputted multiple wrong inputs branch to limit loops
	add $t3, $t3, 1			# Increment counter to keep track of number of wrong inputs
	li $v0, 4				# Displays error message
	la $a0, errmsg
	syscall
	j main					# jumps the user back to the main method so they can re-enter values

limit:						# Only used when the user has inputted multiple wrong methods
	li $v0, 4				
	la $a0, limitmsg		# Prints the limit message
	syscall					
	li $v0, 10				# Ends the program
	syscall
	
end:						 
	li $v0, 4				
	la $a0, result 			# Displays result message
	syscall
	move $a0, $t2			# Prints the Product(t2)
	li $v0, 1
	syscall

li $v0, 10
syscall
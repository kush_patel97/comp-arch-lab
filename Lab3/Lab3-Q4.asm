.data
	prompt: .asciiz "Enter a floating point number: \n"
	cuberoot: .asciiz "\nYour answer is: "

.text
main:

li $v0, 4			# Prompts Message
la $a0, prompt
syscall

li $v0, 6			# Reads in a floating point number and stores it in f0
syscall
mov.s $f1, $f0

#==================================
# e = 0.000001
# f1 = N
# Break loop when |x(i+1) - x(i)| < e
# x(i+1) = (2*x(i) + N/(x(i))^2)/3

li.s $f2, 1.000000		# f2 = 1 = x(i)
li.s $f3, 0.000001		# f3 = epsilion
li.s $f4, 2.000000		# f4 = 2
li.s $f5, 3.000000		# f5 = 3

loop:
	mul.s $f6, $f2, $f4		# f6 = 2*x(i)
	mul.s $f7, $f2, $f2		# f7 = (x(i))^2
	div.s $f8, $f1, $f7		# f8 = N/(x(i))^2
	add.s $f9, $f6, $f8		# f9 = 2*x(i) + N/(x(i))^2
	div.s $f9, $f9, $f5		# f9 = (2*x(i) + N/(x(i))^2)/3
	sub.s $f10, $f9, $f2	# f10 = x(i+1) - x(i)
	abs.s $f11, $f10		# f11 = |x(i+1) - x(i)|
	c.lt.s $f11, $f3		# |x(i+1) - x(i)| < epsilion
	bc1t printAnswer
	mov.s $f2, $f9
	j loop

printAnswer:
	li $v0, 4			# Prints message cuberoot
	la $a0, cuberoot
	syscall
	mov.s $f12, $f9
	li $v0, 2			# Displays answer in floating point
	syscall

li $v0, 10
syscall
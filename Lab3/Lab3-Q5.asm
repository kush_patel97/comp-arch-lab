.data 
r: .asciiz "Enter the circles radius: \n"
L: .asciiz "Enter the slant height: \n"
result: .asciiz "The surface of a circular cone is: "

.text
main:
li $v0, 4
la $a0, r
syscall
		
li $v0, 6	
syscall	
mov.s $f6, $f0

li $v0, 4
la $a0, L
syscall
		
li $v0, 6	
syscall	
mov.s $f7, $f0

#======================================
# f0 = r
# f1 = L
# f2 = 3.14159265359
# f3 = result
# S = pi * r^2 + pi*r*L
li.s $f2, 3.14159265359 
mul.s $f4, $f6, $f6		# f4 = r^2
mul.s $f4, $f4, $f2		# f4= r^2 * pi

mul.s $f3, $f2, $f6		# f3 = pi*r
mul.s $f3, $f3, $f7		# f3 = pi*r*L
add.s $f3, $f3, $f4		# f3 = r^2 * pi + pi*r*L

li $v0, 4				# Prints the result message
la $a0, result
syscall

mov.s $f12, $f3	
li $v0, 2				# Displays floating point answer
syscall

li $v0, 10
syscall
.data
	A: .float 1.355, 2.670, 3.566, 4.560, 5.980, 7.665, 12.340, 18.540
	fzero: .float 0.000
	message: .asciiz "Enter a number: \n"
	sum: .asciiz "Your sum is: "

.text
main:
la $a1, A 			# Value of array A

li $v0, 4
la $a0, message
syscall
		
li $v0, 6	
syscall	
mov.s $f1, $f0

li $t1, 0		# counter to break out of array if it goes through the entire array
li $t2, 8
while:
	l.s $f5, 0($a1)		# A[0]
	c.lt.s $f1, $f5		# If Array value is greater than 
	bc1t end			# Go to end
	beq $t1, $t2, end
	j addloop
	
addloop:
	add.s $f3, $f3, $f5 
	add $a1, $a1, 4 	# Increment index of array
	add $t1, $t1, 1		# Increment counter
	j while

end:
	li $v0, 4
	la $a0, sum
	syscall
	mov.s $f12, $f3
	li $v0, 2
	syscall
	li $v0, 10
	syscall
# Read Input
# Create a while Loop
# while input is greater than the value of that array position, continue
# if the value of the array is greater than my array, break the loop and return the sum

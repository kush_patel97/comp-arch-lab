 .data 0x10000860
vector_A: .word 1,2,3,4,0,0,0,0
 .data 0x10000880
matrix_B: .word 1,2,3,4,5,6,7,8
 .data 0x10000C80
 .word 2,3,4,5,6,7,8,9
 .data 0x10001080
 .word 3,4,5,6,7,8,9,10
 .data 0x10001480
 .word 4,5,6,7,8,9,10,11
 .data 0x10000840
vector_C: .word 0,0,0,0,0,0,0,0
 .text 0x00400000
 .globl main # main program starts in the next line
main:
la $2, vector_A
la $3, matrix_B
la $4, vector_C
li $5, 4
li $6, 8
li $7, 0

L1:
	li $8, 0
	li $9, 0
	add $4, $4, $7
	lw $10, 0($4)

L2:
	li $11, 0
	addi $2, $2, 4
	lw $12, 0($2)
	addi $3, $3, 4
	lw $13, 0($3)
	addi $13, $13, 4
	lw $14, 0($13)
	mult $12, $14
	mflo $v0
	add $8, $8, $11
	addi $9, $5, 12
	add $10, $10, $8
	sw $10, 0($4)
	addi $7, $7, 4
	blt $7, $6, L2
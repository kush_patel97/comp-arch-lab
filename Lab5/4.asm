.data 0x10000800
	OrinRow_0: .word 1,2,3,4,5,6
	OrinRow_1: .word 7,8,9,10,11,12
	OrinRow_2: .word 13,14,15,16,17,18
	OrinRow_3: .word 19,20,21,22,23,24
	OrinRow_4: .word 25,26,27,28,29,30
	OrinRow_5: .word 31,32,33,34,35,36
	ORIG: 		.word OrinRow_0, OrinRow_1, OrinRow_2, OrinRow_3, OrinRow_4, OrinRow_5
.data 0x10000900
	TransRow_0: .word 0,0,0,0,0,0
	TransRow_1: .word 0,0,0,0,0,0
	TransRow_2: .word 0,0,0,0,0,0
	TransRow_3: .word 0,0,0,0,0,0
	TransRow_4: .word 0,0,0,0,0,0
	TransRow_5: .word 0,0,0,0,0,0
	TRANS:      .word TransRow_0, TransRow_1, TransRow_2, TransRow_3, TransRow_4, TransRow_5

.text 0x00400000
main:
#Your code starts from here

li $t6, 0 # i
li $t7, 0 # j

la $t1, ORIG
la $t5, TRANS




loop1:
	beq $t6, 6, exit

	loop2:
		beq $t7, 6, loop1

		sll	$t2, $t6,	2	# Shift left twice (same as i * 4)
		add $s0, $t2, $t1 	# Temp i*4
		add	$t2, $t2, $t1	# Address of pointer A[i]
		lw	$t3, ($t2)		# Get address of an array A[i] and put it into register $t3
		
		sll	$t4, $t7,	2	# Shift left twice (same as j * 4)
		add	$t4, $t3,	$t4	# Address of A[i][j]
		lw	$t0, ($t4)		# Load value of A[i][j]
		

		add $t9, $t4, $t5 	# Address of pointer T[j]
		lw $t8, ($t9)		# Puts value of T[j] and puts it into register $t8
		add $s1, $t8, $s0	# Address of T[j][i]
		lw $s2, ($s1)		# loads T[j][i] into register $s2

		sw $t0, ($s2)

		add $t7, $t7, 1
		j loop2
	
	add $t6, $t6, 1
	j loop1
exit:
	li $v0, 10
	syscall
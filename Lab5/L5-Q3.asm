			.data 0x10000860
vector_A: 	.word 1,2,3,4,0,0,0,0
			.data 0x10000880
matrix_B: 	.word 1,2,3,4,5,6,7,8
			.data 0x10000C80
			.word 2,3,4,5,6,7,8,9
			.data 0x10001080
			.word 3,4,5,6,7,8,9,10
			.data 0x10001480
			.word 4,5,6,7,8,9,10,11
			.data 0x10000840
vector_C: 	.word 0,0,0,0,0,0,0,0

.text 0x00400000
.globl main 		# main program starts in the next line
main:
	la $2, vector_A
	la $3, matrix_B
	la $4, vector_C
	li $5, 4		# Length of j
	li $6, 8		# Length of i
	li $7, 0		# j
	li $11, 0		# increment index i
	li $12, 0		# increment index j	

loop1:
	beq $8, $6, exit	# if i == 8
	li $8, 0		# i
	li $9, 0		# sum
		

loop2:
	beq $5, $7, loop3	# if j == 4
	lw $t13, $12($2)		# vector_A[j]
	
	lw $14, $12($3)		# matrix_B[j]
	lw $15, $11($3)		# matrix_B[i]
	add $16, $14, $15	# matrix_B[j][i]
	lw $17, ($16)		# $17 = matrix_B[j][i] 
	mul $18, $13, $17	# mul_result = vector_A[j] + matrix_B[j][i]
	add $9, $9, $18		# sum = sum + mul_result
	
	add $12, $12, 4		# increase index of array by 4
	add $7, $7, 1		# j++
	j loop2

loop3:
	sw $9, $11($4)
	add $11, $11, 4	# Starts $4 at 0
	add $8, $8, 1 	# i++
	j loop1

exit:
	li $v0, 10
	syscall
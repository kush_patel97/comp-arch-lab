.data 0x10000800
	OrinRow_0: .word 1 2 3 4 5 6
	OrinRow_1: .word 7 8 9 10 11 12
	OrinRow_2: .word 13 14 15 16 17 18
	OrinRow_3: .word 19 20 21 22 23 24
	OrinRow_4: .word 25 26 27 28 29 30
	OrinRow_5: .word 31 32 33 34 35 36
.data 0x010010000
	TransRow_0: .word 0 0 0 0 0 0
	TransRow_1: .word 0 0 0 0 0 0
	TransRow_2: .word 0 0 0 0 0 0
	TransRow_3: .word 0 0 0 0 0 0
	TransRow_4: .word 0 0 0 0 0 0
	TransRow_5: .word 0 0 0 0 0 0

# formatting
#Declare new line
newline: .asciiz "\n"

#Declare space
printspace: .asciiz " "

.text 0x00400000

main:

la $t0, OrinRow_0 	
la $t1, TransRow_0 	
la $t2, TransRow_1 	
la $t3, TransRow_2 	
la $t4, TransRow_3	
la $t5, TransRow_4 	
la $t6, TransRow_5 	


li $t7, 0		# Counter
li $s0, 6		# Size of the rows and columns


Row:
	beq $t7, $s0, EndRow # if $s0 == $t7 then end loop
	lw $t9, 0($t0) 	# Load first value from matrix
	sw $t9, 0($t1) 	# Store first value into transposed matrix
	add $t0, $t0, 4 # add 4 to go to next index
	lw $t9, 0($t0) 
	sw $t9, 0($t2) 
	add $t0, $t0, 4 
	lw $t9, 0($t0) 
	sw $t9, 0($t3) 
	add $t0, $t0, 4 
	lw $t9, 0($t0) 
	sw $t9, 0($t4) 
	add $t0, $t0, 4			
	lw $t9, 0($t0) 
	sw $t9, 0($t5) 
	add $t0, $t0, 4 
			
	lw $t9, 0($t0) 
	sw $t9, 0($t6) 
		
	add $t0, $t0, 4
	# add 4 to transpose matrix column to move onto next index in the matrix
	add $t1, $t1, 4
	add $t2, $t2, 4
	add $t3, $t3, 4
	add $t4, $t4, 4
	add $t5, $t5, 4
	add $t6, $t6, 4
	add $t7, $t7, 1 # increment t7 to move onto next row
	j Row


EndRow: 
	la $t1, TransRow_0 # Load the transposed matrix
	li $t0, 5 # boundaries
	li $t7, 0 
	
transrow:
		
	bgt $t7, $t0, exit # if $t7 == $t0 branch to exit
	li $v0, 4 
	la $a0, newline 
	syscall
	li $t2, 0 
		
trans2: 
			
	beq $t2, $t0, endtrans2 # if $t2 == $t0 branch to endtrans2
	li $v0,1 
	lw $a0, ($t1) 
	syscall
	li $v0, 4 
	la $a0, printspace 
	syscall
	add $t2, $t2, 1 
	add $t1, $t1, 4 
				
	j trans2 

endtrans2: 
	li $v0,1 
	lw $a0, ($t1) 
	syscall	
	add $t1, $t1, 4 # increment to go to next row index
	add $t7, $t7, 1 # increment to go to next row
			
	j transrow 

exit:
	li $2, 10
	syscall
 	
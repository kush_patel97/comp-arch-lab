forloop:
	add.s32 r1, 0, 40;			// r0 = 4*10
	add.s32 r2, 0, 0;			// i = 0
	setp.eq.s32 r9, r2, r1;		//if i = 40 Break out of loop
	@r9 bra exit;

	setp.lt.s32 r3, r5, r6;		// if a < b
	@r3 bra if;

	setp.gt.s32 r4, r5, r6;		// if a > b
	@r3 bra elif;

	setp.eq.s32 r3, r5, r6;		// if a = b
	@r3 bra else;

	if:
		add.s32 r11, r6, r8;		 // temp = a + c
		st.local.s32 [r10 + r2], r11;// e[i] = temp
		add.s32 r2, r2, 4;			 // i = i + 4
		@r2 bra forloop;
	elif:
		add.s32 r11, r7, r9;		 // temp = b + d
		st.local.s32 [r10 + r2], r11;// e[i] = temp
		add.s32 r2, r2, 4;			 // i = i + 4
		@r2 bra forloop;
	else:
		mul.s32 r11, r6, r9;		 // temp = a * d
 		st.local.s32 [r10 + r2], r11;// e[i] = temp
		add.s32 r2, r2, 4;			 // i = i + 4
		@r2 bra forloop;

	

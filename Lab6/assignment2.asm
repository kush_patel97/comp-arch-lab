// r0 = Array[]
add.s32 r2, 0, 0			//r2 = 0
add.s32 r3, 0, 0			//r3 = even sum
add.s32 r4, 0, 0 			//r4 = odd sum
add.f32 r7, 0, 0			//r7 = even counter
add.f32 r8, 0, 0			//r8 = odd counter
forloop:
	setp.gt.s32 r9, r2, 36;		// if r2 > 36 because that is the 10th item in the list
	@r9 bra average;				// Exit the loop
	ld.global.s32 r5, [r0 + r2];// r5 = Arrau[i], initially Array[0]
	rem.s32 r6, r5, 2;			// r5 mod 2
	setp.eq.s32 r9, r6, 0;		// if r5 == 0
	@r9 bra even;
	setp.eq.s32 r9, r6, 1;		// if r5 == 1
	@r9 bra odd;
	bra forloop;

even:
	add.s32 r3, r3, r5;			//sum of even numbers
	add.s32 r2, r2, 4;			//r2 = r2 + 4 offset increment
	add.f32 r7, r7, 1.0;			//r7 = r7 + 1
	bra forloop;

odd:
	add.s32 r4, r4, r5;			//sum of odd numbers
	add.s32 r2, r2, 4;			//r2 = r2 + 4 offset increment
	add.f32 r8, r8, 1.0;			//r8 = r8 + 1.0
	bra forloop;

average:
	cvt.s32.f32 r3, r3			//Convert the sum of even numbers to floating point
	cvt.s32.f32 r4, r4			//Convert the sum of odd numbers to float point
	div.f32 r3, r3, r7			//r3 = sum/number of even numbers
	div.f32 r4, r4, r8			//r3 = sum/number of odd numbers

	//r3 represents the average of all even numbers
	//r4 represents the average of all odd numbers
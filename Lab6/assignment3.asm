.data 
num1: .asciiz "Enter a number: \n"
pi: .asciiz "\nYour result:\n"

# f1 = n max number
# f2 = k = 1
# f3 = 1
# f4 = answer

.text
main:
	li $v0, 4			# Prints out num1 string using syscall
	la $a0, num1		
	syscall				

	li $v0, 6			# Reads in an input
	syscall				
	mov.s $f1, $f0		# Moves the input to a temp register
	
	li.s $f2, 1.0		# f2 = k = 1
	li.s $f3, 1.0		# f3 = 1
	neg.s $f5, $f3		# f5 = -1
	neg.s $f7, $f3		# f7 = -1
	li.s $f8, 4.0		# f8 = 4	
	li.s $f4, 0.0		# $f4 = 0
loop:
	c.le.s $f2, $f1 	# if f2 > f1
	bc1f exit
	add.s $f6, $f2, $f2	# f6 = k + k = 2*k
	sub.s $f6, $f6, $f3	# f6 = 2k - 1
	div.s $f6, $f3, $f6	# f6 = 1/2k-1
	mul.s $f5, $f5, $f7	# f5 = -1 * -1
	mul.s $f6, $f6, $f5	# f6 = -1(1/2k-1)
	add.s $f4, $f4, $f6
	add.s $f2, $f2, $f3	# k = k + 1
	j loop

exit:					# exits the program
	mul.s $f4, $f4, $f8

	li $v0, 4			# Prints out pi string using syscall
	la $a0, pi		
	syscall	

	mov.s $f12, $f4	
	li $v0, 2			# Displays floating point answer
	syscall

	li $v0, 10
	syscall

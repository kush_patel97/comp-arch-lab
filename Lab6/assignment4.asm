.data
arr: .word 12 -4 9 7 11 8
result: .asciiz "Sorted: \n"
space: .asciiz " "

.text
.globl main

main:
	li $s0, 5
	li $t1, 0
	la $a0, arr
	li $a1, 6
	jal bs
	j print

bs:
	li $t1, 0
	loop1:
		addi $t1, $t1, 1
		bgt $t1, $a1, reset
		li $t0, 6
	
	loop2:
		bge $t1, $t0, loop1
		addi $t0, $t0, -1
		mul $t4, $t0, 4
		addi $t3, $t4, -4	
		add $t7, $t4, $a0
		add $t8, $t3, $a0
		lw $t5, 0($t7)
		lw $t6, 0($t8)
		bgt $t5, $t6, loop2
		sw $t5, 0($t8)
		sw $t6, 0($t7)
		j loop2
	
	reset:
		jr $ra

	print:
		li $v0, 4
		la $a0, result
		syscall
		la $t1, arr
		li $t0,0

	loop3:
		lw $a0, 0($t1)
		li $v0, 1
		syscall

		li $v0, 4
		la $a0, space
		syscall

		addi $t1, $t1, 4
		addi $t0, $t0, 1
		slt $t2, $s0, $t0
		beq $t2, $0, loop3
		li $v0, 10
		syscall
add.f32 r1, 0, n;
add.f32 r2, 0, 1.0;

loop:
	setp.gt.f32 r3, r2, r1;	//if k > n 
	@r3 bra end; 			//Break loop

	mul.f32 r4, r2, 2.0;	//2k
	sub.f32 r4, r4, 1.0;		//2k-1
	div.f32 r4, 1.0, r4;		//1/(2k-1)
	neg.f32 r5, 1.0;		//-1
	neg.f32 r7, 1.0;		//-1
	mul.f32 r5, r5, r7;		//r5 = -1 * -1
	mul.f32 r6, r5, r4;		//r6 = r5/(2k-1) 
	add.f32 r2, r2, 1.0;	//k++
	@r3 bra loop;

end:
	mul.f32 r6, 4.0, r6;	//r6 = 4(r5)/2k-1
	@r3 bra exit
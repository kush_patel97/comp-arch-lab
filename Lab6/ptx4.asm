add.s32 r1, 0, 1	// r1 = 1	
sub.s32 r3, 0, 1	// r3 = -1

loop1:
	add.s32 r3, 0, 1		//i = i+1
	setp.eq.s32 r5, r1, 0 	//if r1==0
	@r4 bra reset1			//Branch to reset1
	setp.ge.s32 r5, r3, r1 	//if r3>=r1
	@r5 bra reset1			//branch to reset1

	sub.s32 r1, r1, 1		//a = 0
	sub.s32 r4, 0, 1		//j = -1

loop2:
	add.s32 r3, 0, 1		//j = j + 1
	setp.ge.s32 r5, r3, r1 	// if r3>=r1
	@r5 bra loop1

	mul.s32 r5, r3, 4 		//j*4
	ld.global.s32 r6, [r9 + r5]
	add.s32 r5, r5, 4
	ld.global.s32 r7, [r9 + r5]

	setp.ge.s32 r4, r7, r6	//r7 >= r6
	@r4 bra loop2
	add.s32 r8, r6, 0
	add.s32 r6, r7, 0
	add.s32 r7, r8, 0
	mul.s32 r5, r3, 4
	ld.global.s32 r6, [r9 + r5]
	add.s32 r5, r5, 4
	ld.global.s32 r7, [r9 + r5]

	add.s32 r0, 0, 1
	@r0, bra loop2

	reset1:
	exit: